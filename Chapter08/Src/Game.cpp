// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "Game.h"
#include "Texture.h"
#include "VertexArray.h"
#include "InputSystem.h"
#include "Asteroid.h"
#include "Actor.h"
#include "SpriteComponent.h"
#include "Ship.h"
#include <algorithm>


// Generated inlcludes
#include "Sprite_bj_vpo.h"
#include "Sprite_bj_fpo.h"

Game::Game()
:mInputSystem(nullptr)
,mWindow(nullptr)
,mIsRunning(true)
,mUpdatingActors(false)
,mScreenWidth(0)
,mScreenHeight(0)
{
}

bool Game::Initialize()
{
	//if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_JOYSTICK) != 0)
    if (SDL_Init(SDL_INIT_JOYSTICK) != 0)
	{
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}

    int flags = IMG_INIT_JPG|IMG_INIT_PNG;
    if (IMG_Init(flags) & flags != flags) {
        SDL_Log("Failed to init IMG: %s\n", IMG_GetError());
    }

    //mWindow = SDL_SetVideoMode(mScreenWidth, mScreenHeight, 0, SDL_FULLSCREEN);
    //if (!mWindow) {
    //    SDL_Log("Failed to set video mode: %s\n", SDL_GetError());
    //    return false;
    //}
    if (!mRenderer.Init()) {
        SDL_Log("Failed to init renderer");
    }
    mScreenWidth = mRenderer.GetScreenWidth();
    mScreenHeight = mRenderer.GetScreenHeight();
    SDL_Log("Renderer width, height: %d, %d\n", mScreenWidth, mScreenHeight);
    
    // Initialize input system
    mInputSystem = new InputSystem();
    if (!mInputSystem->Initialize() ||
        !mInputSystem->ParseMappingFile(ASSETS_DIR"/Cfg/Input.cfg")) {
        SDL_LogError(0, "Failed to initialize input system");
        return false;
    }

    // Make sure we can create/compile shaders
    if (!LoadShaders())
    {
        SDL_Log("Failed to load shaders.");
        return false;
    }

    // Create quad for drawing sprites
    CreateSpriteVerts();

    LoadData();

	mTicksCount = SDL_GetTicks();
	
	return true;
}

void Game::RunLoop()
{
	while (mIsRunning)
	{
		ProcessInput();
		UpdateGame();
		GenerateOutput();
	}
}

void Game::ProcessInput()
{
    mInputSystem->PrepareForUpdate();
    
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				mIsRunning = false;
				break;
			case SDL_MOUSEWHEEL:
/*            case SDL_JOYDEVICEADDED:
            case SDL_JOYDEVICEREMOVED:*/
				mInputSystem->ProcessEvent(event);
				break;
            /*case SDL_JOYAXISMOTION:
                if (event.jaxis.value > 10000 || event.jaxis.value < -10000)
                    SDL_Log("Joy axis motion: which: %d, axis: %d, value: %d\n", 
                            event.jaxis.which,
                            event.jaxis.axis,
                            event.jaxis.value);
                break;*/
			default:
				break;
		}
	}
    
    mInputSystem->Update();
    const InputState& state = mInputSystem->GetState();

    if (state.Controllers[0].GetButtonState(PS3_BUTTON_START) == EReleased) {
        mIsRunning = false;
    }

    mUpdatingActors = true;
    for (auto actor : mActors)
    {
        actor->ProcessInput(*mInputSystem);
    }
    mUpdatingActors = false;
}

void Game::UpdateGame()
{
	// Compute delta time
	// Wait until 16ms has elapsed since last frame
    while (SDL_GetTicks() < mTicksCount + 16)
    {
        SDL_Delay((mTicksCount + 16) - SDL_GetTicks());
    }

	float deltaTime = (SDL_GetTicks() - mTicksCount) / 1000.0f;
	if (deltaTime > 0.05f)
	{
		deltaTime = 0.05f;
	}
	mTicksCount = SDL_GetTicks();

    // Update all actors
    mUpdatingActors = true;
    for (auto actor : mActors)
    {
        actor->Update(deltaTime);
    }
    mUpdatingActors = false;

    // Move any pending actors to mActors
    for (auto pending : mPendingActors)
    {
        pending->ComputeWorldTransform();
        mActors.emplace_back(pending);
    }
    mPendingActors.clear();

    // Add any dead actors to a temp vector
    std::vector<Actor*> deadActors;
    for (auto actor : mActors)
    {
        if (actor->GetState() == Actor::EDead)
        {
            deadActors.emplace_back(actor);
        }
    }

    // Delete dead actors (which removes them from mActors)
    for (auto actor : deadActors)
    {
        delete actor;
    }
}

void Game::GenerateOutput()
{
    mRenderer.BeginFrame();

    mSpriteShader.Use();
    for (auto sprite : mSprites)
    {
        sprite->Draw( &mSpriteShader );
    }

    mRenderer.EndFrame();
}

bool Game::LoadShaders()
{
    if (!mSpriteShader.Init( mRenderer.GetGcmContext(),
                             (rsxVertexProgram*)Sprite_bj_vpo,
                             (rsxFragmentProgram*)Sprite_bj_fpo )) {
        SDL_Log("Failed to init shader");
        return false;
    }
    mSpriteShader.Use();
    Matrix4 viewProj = Matrix4::CreateSimpleViewProj(mRenderer.GetScreenWidth(), mRenderer.GetScreenHeight());
    mSpriteShader.SetMatrixUniform("uViewProj", viewProj);

    return true;
}

void Game::CreateSpriteVerts()
{
	float vertices[] = {
		-0.5f,  0.5f, 0.f, 0.f, 0.f, // top left
		 0.5f,  0.5f, 0.f, 1.f, 0.f, // top right
		 0.5f, -0.5f, 0.f, 1.f, 1.f, // bottom right
		-0.5f, -0.5f, 0.f, 0.f, 1.f  // bottom left
	};

	unsigned int indices[] = {
		0, 1, 2,
		2, 3, 0
	};

    // TODO - move into renderer or vertexArray class or something
    mMeshBuffer.cnt_vertices = 4;
    mMeshBuffer.cnt_indices = 6;
    mMeshBuffer.vertices = (PS3::S3DVertex*)rsxMemalign( 128, mMeshBuffer.cnt_vertices * sizeof(PS3::S3DVertex));
    mMeshBuffer.indices = (u16*)rsxMemalign(128, mMeshBuffer.cnt_indices * sizeof(u16));
    if (!mMeshBuffer.vertices || !mMeshBuffer.indices) {
        SDL_Log("Failed to allocate sprite vertices");
        return;
    }

    for (int i=0; i < mMeshBuffer.cnt_indices; ++i) {
        mMeshBuffer.indices[i] = indices[i];
    }
    for (int i=0; i < mMeshBuffer.cnt_vertices; ++i) {
        float* vtx = &vertices[i*5];
        mMeshBuffer.vertices[i] = PS3::S3DVertex( vtx[0], // position
                                             vtx[1],
                                             vtx[2],
                                             0,0,0,  // normals
                                             vtx[3], // tex coords
                                             vtx[4] );
        SDL_Log("Vertex: %f,%f,%f\n", vtx[0], vtx[1], vtx[2]);
    }
}

void Game::LoadData()
{
    // Create player's ship
    mShip = new Ship(this);
    mShip->SetRotation(Math::PiOver2);

    // Create asteroids
    const int numAsteroids = 20;
    for (int i = 0; i < numAsteroids; ++i)
    {
        new Asteroid(this);
    }
}

void Game::UnloadData()
{
    // Because ~Actor calls RemoveActor, we have to use a different style loop
    while (!mActors.empty())
    {
        delete mActors.back();
    }

    // Destroy textures
    for (auto i : mTextures)
    {
        i.second->Unload();
        delete i.second;
    }
    mTextures.clear();
}

Texture* Game::GetTexture(const std::string& fileName)
{
    Texture* tex = nullptr;
    auto iter = mTextures.find(fileName);
    if (iter != mTextures.end())
    {
        tex = iter->second;
    }
    else
    {
        tex = new Texture();
        if ( tex->Load( mRenderer.GetGcmContext(),
                        fileName ))
        {
            mTextures.emplace( fileName, tex );
        }
        else
        {
            delete tex;
            tex = nullptr;
        }
    }
    return tex;
}

void Game::AddAsteroid(Asteroid* ast)
{
    mAsteroids.emplace_back(ast);
}

void Game::RemoveAsteroid(Asteroid* ast)
{
    auto iter = std::find(mAsteroids.begin(), mAsteroids.end(), ast);
    if (iter != mAsteroids.end())
    {
        mAsteroids.erase(iter);
    }
}

void Game::Shutdown()
{
	UnloadData();
    
    mInputSystem->Shutdown();
    delete mInputSystem;

    SDL_FreeSurface(mWindow);
    IMG_Quit();
	SDL_Quit();
}

void Game::AddActor(Actor* actor)
{
    SDL_Log("Add actor");
    // If we're updating actors, need to add to pending
    if (mUpdatingActors)
    {
        SDL_Log("  Placing in pending actors");
        mPendingActors.emplace_back(actor);
    }
    else
    {
        SDL_Log("  Placing in actors");
        mActors.emplace_back(actor);
    }
}

void Game::RemoveActor(Actor* actor)
{
	// Is it in pending actors?
	auto iter = std::find(mPendingActors.begin(), mPendingActors.end(), actor);
	if (iter != mPendingActors.end())
	{
		// Swap to end of vector and pop off (avoid erase copies)
		std::iter_swap(iter, mPendingActors.end() - 1);
		mPendingActors.pop_back();
	}

	// Is it in actors?
	iter = std::find(mActors.begin(), mActors.end(), actor);
	if (iter != mActors.end())
	{
		// Swap to end of vector and pop off (avoid erase copies)
		std::iter_swap(iter, mActors.end() - 1);
		mActors.pop_back();
	}
}

void Game::AddSprite(SpriteComponent* sprite)
{
    // Find the insertion point in the sorted vector
    // (The first element with a higher draw order than me)
    int myDrawOrder = sprite->GetDrawOrder();
    auto iter = mSprites.begin();
    for (; iter != mSprites.end(); ++iter)
    {
        if (myDrawOrder < (*iter)->GetDrawOrder()) {
            break;
        }
    }

    // Inserts element before position of iterator
    mSprites.insert(iter, sprite);
}

void Game::RemoveSprite(SpriteComponent* sprite)
{
    auto iter = std::find(mSprites.begin(), mSprites.end(), sprite);
    mSprites.erase(iter);
}

