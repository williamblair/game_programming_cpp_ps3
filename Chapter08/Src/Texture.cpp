// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "Texture.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

Texture::Texture()
:mContext(nullptr)
,mBuffer(nullptr)
,mOffset(0)
,mWidth(0)
,mHeight(0)
{
	
}

Texture::~Texture()
{
	
}

bool Texture::Load( gcmContextData* context,
                    const std::string& fileName )
{
#if 0
	int channels = 0;
	
	unsigned char* image = SOIL_load_image(fileName.c_str(),
										   &mWidth, &mHeight, &channels, SOIL_LOAD_AUTO);
	
	if (image == nullptr)
	{
		SDL_Log("SOIL failed to load image %s: %s", fileName.c_str(), SOIL_last_result());
		return false;
	}
	
	int format = GL_RGB;
	if (channels == 4)
	{
		format = GL_RGBA;
	}
	
	glGenTextures(1, &mTextureID);
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	
	glTexImage2D(GL_TEXTURE_2D, 0, format, mWidth, mHeight, 0, format,
				 GL_UNSIGNED_BYTE, image);
	
	SOIL_free_image_data(image);
	
	// Enable bilinear filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#endif

    mContext = context;

    SDL_Surface* surface = nullptr;
//    if (mSurface != nullptr) {
//        SDL_Log("Texture::Load: surface wasn't null, freeing first");
//        SDL_FreeSurface(mSurface);
//    }
    surface = IMG_Load(fileName.c_str());
    if ( !surface ) {
        SDL_LogError(0, "Texture::Load: failed to load image: %s", IMG_GetError());
        return false;
    }    

    mWidth = surface->w;
    mHeight = surface->h;

    mBuffer = (u32*)rsxMemalign( 128, mWidth * mHeight * 4 ); // force 4 bytes per pixel
    if ( !mBuffer ) {
        SDL_LogError(0, "Texture::Load: failed to allocate rsx buffer");
        SDL_FreeSurface(surface);
        return false;
    }
    rsxAddressToOffset( mBuffer, &mOffset );

    SDL_LockSurface( surface );

    u8* buffer = (u8*)mBuffer;
    u8* pixels = (u8*)surface->pixels;
    for ( u32 i = 0; i < mWidth * mHeight * 4; i += 4 )
    {
        buffer[ i+1 ] = *pixels++; // r
        buffer[ i+2 ] = *pixels++; // g
        buffer[ i+3 ] = *pixels++; // b
        if ( surface->format->BytesPerPixel == 4 ) {
            buffer[ i+0 ] = *pixels++; // a
        } else {
            buffer[ i+0 ] = 255;
        }
        //SDL_Log("tex alpha: %d\n", buffer[i+0]);
    }

    SDL_UnlockSurface( surface );
    SDL_FreeSurface( surface );

	return true;
}

void Texture::Unload()
{
	//glDeleteTextures(1, &mTextureID);
    //SDL_FreeSurface(mSurface);
    //mSurface = nullptr;
    rsxFree( mBuffer );
    mBuffer = nullptr;
}

void Texture::SetActive( u8 shaderTexUnit )
{
    // TODO - or just remove
	//glBindTexture(GL_TEXTURE_2D, mTextureID);
    u32 width = mWidth;
    u32 height = mHeight;
    u32 pitch = width * 4;

    //DBG_ASSERT( mBuffer != nullptr );

    rsxInvalidateTextureCache( mContext, GCM_INVALIDATE_TEXTURE );
    
    mTexture.format       = (GCM_TEXTURE_FORMAT_A8R8G8B8 | GCM_TEXTURE_FORMAT_LIN);
    mTexture.mipmap       = 1;
    mTexture.dimension    = GCM_TEXTURE_DIMS_2D;
    mTexture.cubemap      = GCM_FALSE;
    mTexture.remap        = ((GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_B_SHIFT) |
                           (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_G_SHIFT) |
                           (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_R_SHIFT) |
                           (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_A_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_B << GCM_TEXTURE_REMAP_COLOR_B_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_G << GCM_TEXTURE_REMAP_COLOR_G_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_R << GCM_TEXTURE_REMAP_COLOR_R_SHIFT) |
                           (GCM_TEXTURE_REMAP_COLOR_A << GCM_TEXTURE_REMAP_COLOR_A_SHIFT));
    mTexture.width       = width;
    mTexture.height      = height;
    mTexture.depth       = 1;
    mTexture.location    = GCM_LOCATION_RSX;
    mTexture.pitch       = pitch;
    mTexture.offset      = mOffset;
    rsxLoadTexture(mContext,shaderTexUnit,&mTexture);
    rsxTextureControl(mContext,shaderTexUnit,GCM_TRUE,0<<8,12<<8,GCM_TEXTURE_MAX_ANISO_1);
    rsxTextureFilter(mContext,shaderTexUnit,0,GCM_TEXTURE_LINEAR,GCM_TEXTURE_LINEAR,GCM_TEXTURE_CONVOLUTION_QUINCUNX);
    //rsxTextureWrapMode(mContext,shaderTexUnit,GCM_TEXTURE_CLAMP_TO_EDGE,GCM_TEXTURE_CLAMP_TO_EDGE,GCM_TEXTURE_CLAMP_TO_EDGE,0,GCM_TEXTURE_ZFUNC_LESS,0);
    rsxTextureWrapMode(mContext,shaderTexUnit,GCM_TEXTURE_REPEAT,GCM_TEXTURE_REPEAT,GCM_TEXTURE_REPEAT,0,GCM_TEXTURE_ZFUNC_LESS,0);
}

