// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <unordered_map>
#include <string>
#include <vector>
#include "Math.h"
#include "Renderer.h"
#include "Shader.h"


class Game
{
public:
	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

    void AddActor(class Actor* actor);
    void RemoveActor(class Actor* actor);

    void AddSprite(class SpriteComponent* sprite);
    void RemoveSprite(class SpriteComponent* sprite);

    class Texture* GetTexture(const std::string& fileName);

    SDL_Surface* GetWindowSurface() { return mWindow; }
    int GetScreenWidth() const { return mScreenWidth; }
    int GetScreenHeight() const { return mScreenHeight; }

    PS3::Renderer* GetRenderer() { return &mRenderer; }
    PS3::SMeshBuffer* GetSpriteMeshBuffer() { return &mMeshBuffer; }

    // Game specific
    void AddAsteroid(class Asteroid* ast);
    void RemoveAsteroid(class Asteroid* ast);
    std::vector<class Asteroid*>& GetAsteroids() { return mAsteroids; }

private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();
	bool LoadShaders();
	void CreateSpriteVerts();
	void LoadData();
	void UnloadData();

    // Map of textures loaded
    std::unordered_map<std::string, class Texture*> mTextures;

    // All the actors in the game
    std::vector<class Actor*> mActors;
    
    PS3::Renderer mRenderer;
    class InputSystem* mInputSystem;

    // Any pending actors
    std::vector<class Actor*> mPendingActors;

    // All the sprite components drawn
    std::vector<class SpriteComponent*> mSprites;

    // Sprite shader
    PS3::Shader mSpriteShader;
    // TODO
    // VertexArray mSpriteVerts
    PS3::SMeshBuffer mMeshBuffer;
    
    SDL_Surface* mWindow;
	Uint32 mTicksCount;
	bool mIsRunning;
    // Track if we're updating actors right now
    bool mUpdatingActors;
    int mScreenWidth;
    int mScreenHeight;

    // Game specific
    class Ship* mShip;
    std::vector<class Asteroid*> mAsteroids;
};
