// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "Shader.h"

namespace PS3
{

Shader::Shader() :
    mContext( nullptr ),
    mVpo( nullptr ),
    mFpo( nullptr ),
    mVpUCode( nullptr ),
    mFpUCode( nullptr ),
    mFpBuffer( nullptr ),
    mFpOffset( 0 )
{
}

Shader::~Shader()
{
}

bool Shader::Init( gcmContextData* context,
                   rsxVertexProgram* vpo,
                   rsxFragmentProgram* fpo )
{
    //DBG_ASSERT( context != nullptr );
    //DBG_ASSERT( vpo != nullptr );
    //DBG_ASSERT( fpo != nullptr );

    mContext = context;
    mVpo = vpo;
    mFpo = fpo;

    bool res = InitUCode();

    return res;
}

bool Shader::InitUCode()
{
    u32 fpsize = 0;
    u32 vpsize = 0;

    rsxVertexProgramGetUCode( mVpo, &mVpUCode, &vpsize );
    //DBG_LOG( "vpsize: %d\n", vpsize );

    rsxFragmentProgramGetUCode( mFpo, &mFpUCode, &fpsize );
    //DBG_LOG( "fpsize: %d\n", fpsize );

    mFpBuffer = (u32*)rsxMemalign( 64, fpsize );
    memcpy( mFpBuffer, mFpUCode, fpsize );
    rsxAddressToOffset( mFpBuffer, &mFpOffset );
    //DBG_LOG( "fp buffer offset: %u\n", mFpOffset );

    return true;
}

} // end namespace PS3

