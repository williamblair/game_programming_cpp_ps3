// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include <string>
#include "Math.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ppu-types.h>
#include <sys/process.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

namespace PS3
{

class Shader
{
public:
	Shader();
	~Shader();

    bool Init( gcmContextData* context,
               rsxVertexProgram* vpo, 
               rsxFragmentProgram* fpo );

    void Use() {
        rsxLoadVertexProgram( mContext, mVpo, mVpUCode );
        rsxLoadFragmentProgramLocation( mContext, mFpo, mFpOffset, GCM_LOCATION_RSX );
    }

    rsxProgramAttrib* GetFragmentProgAttrib( const char* name )
    {
        return rsxFragmentProgramGetAttrib( mFpo, name );
    }

    void SetVertexProgParam( const char* name, float* val )
    {
        rsxProgramConst* prgCnst = rsxVertexProgramGetConst( mVpo, name );
        if ( prgCnst == nullptr )
        {
            printf( "%s:%d: failed to get vertex prog const: %s\n",
                    __FILE__,
                    __LINE__,
                    name );
        }
        else
        {
            rsxSetVertexProgramParameter( mContext, mVpo, prgCnst, val );
        }
    }

    void SetMatrixUniform( const char* uni, Matrix4& mat )
    {
        //Matrix4 t = Matrix4::Transpose(mat);
        SetVertexProgParam( uni, (float*)mat.GetAsFloatPtr() );
    }

private:

    gcmContextData* mContext;
    rsxVertexProgram* mVpo;
    rsxFragmentProgram* mFpo;
    
    void* mVpUCode;
    void* mFpUCode;

    u32* mFpBuffer;
    u32  mFpOffset;

    bool InitUCode();
};

} // end namespace PS3

