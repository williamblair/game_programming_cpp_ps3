// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <ppu-types.h>
#include <rsx/rsx.h>

class Texture
{
public:
	Texture();
	~Texture();
	
	bool Load( gcmContextData* context,
               const std::string& fileName );
	void Unload();
	
	void SetActive( u8 shaderTexUnit );

	int GetWidth() const { return mWidth; }
	int GetHeight() const { return mHeight; }
private:
    
    gcmContextData* mContext;
    
    u32* mBuffer;
    u32  mOffset;

    u32 mWidth;
    u32 mHeight;

    gcmTexture mTexture;
};
