// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "SpriteComponent.h"
#include "Texture.h"
#include "Actor.h"
#include "Game.h"
#include "Shader.h"

SpriteComponent::SpriteComponent(Actor* owner, int drawOrder)
	:Component(owner)
	,mTexture(nullptr)
	,mDrawOrder(drawOrder)
	,mTexWidth(0)
	,mTexHeight(0)
{
	mOwner->GetGame()->AddSprite(this);
}

SpriteComponent::~SpriteComponent()
{
	mOwner->GetGame()->RemoveSprite(this);
}

void SpriteComponent::Draw(PS3::Shader* shader)
{
#if 1
	if (mTexture)
	{
        if ( !shader->GetFragmentProgAttrib("uTexture") ) {
            SDL_Log( "Failed to get uTexture" );
        }
        u8 texIndex = shader->GetFragmentProgAttrib("uTexture")->index;

		// Scale the quad by the width/height of texture
		Matrix4 scaleMat = Matrix4::CreateScale(
			static_cast<float>(mTexWidth),
			static_cast<float>(mTexHeight),
			1.0f);
		
		Matrix4 world = scaleMat * mOwner->GetWorldTransform();
		
		// Since all sprites use the same shader/vertices,
		// the game first sets them active before any sprite draws
		
		// Set world transform
		shader->SetMatrixUniform( "uWorldTransform", world );
		// Set current texture
		mTexture->SetActive( texIndex );
		// Draw quad
        // TODO
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
        mOwner->GetGame()->GetRenderer()->DrawMeshBuffer(
            *(mOwner->GetGame()->GetSpriteMeshBuffer())
        );
	}
#endif

#if 0
    if (mTexture)
    {
        const Vector2& ownerPos = mOwner->GetPosition();
        SDL_Surface* winSurface = mOwner->GetGame()->GetWindowSurface();
        SDL_Rect posRect{ (int)ownerPos.x, (int)ownerPos.y, mTexWidth, mTexHeight };
        SDL_BlitSurface(mTexture->GetSurface(), nullptr, winSurface, &posRect);
    }
#endif
}

void SpriteComponent::SetTexture(Texture* texture)
{
	mTexture = texture;
	// Set width/height
	mTexWidth = texture->GetWidth();
	mTexHeight = texture->GetHeight();
}
