#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>
#include <vectormath/cpp/vectormath_aos.h>

namespace PS3
{

//using namespace Vectormath::Aos;
/*
template< class T >
inline const T& min_(const T& a,const T& b)
{
	return a<b?a:b;
}

template< class T >
inline const T& max_(const T& a,const T& b)
{
	return a<b?b:a;
}

template< class T >
inline const T clamp(const T& val,const T& low,const T& high)
{
	return min_(max_(val,low),high);
}
*/
struct S3DVertex
{
	S3DVertex() {};
	S3DVertex(f32 x,f32 y,f32 z,f32 nx,f32 ny,f32 nz,f32 tu,f32 tv)
		: pos(x,y,z),nrm(nx,ny,nz),u(tu),v(tv) {};

	inline S3DVertex& operator=(const S3DVertex& other)
	{
		pos = other.pos;
		nrm = other.nrm;
		u = other.u;
		v = other.v;
		return *this;
	}

	Vectormath::Aos::Vector3 pos;
	Vectormath::Aos::Vector3 nrm;

	f32 u,v;
};

template< class T >
class CMeshBuffer
{
public:
	CMeshBuffer() : indices(NULL),indices32(NULL),cnt_indices(0),vertices(NULL),cnt_vertices(0) {};

	u16 *indices;
    u32 *indices32; 
	u32 cnt_indices;

	S3DVertex *vertices;
	u32 cnt_vertices;
};

typedef CMeshBuffer<S3DVertex> SMeshBuffer;

class Renderer
{
public:

    Renderer();
    ~Renderer();

    bool Init();

    void SetDrawEnv();

    void BeginFrame();
    void EndFrame();

    gcmContextData* GetGcmContext() { return mContext; }

    int GetScreenWidth() const;
    int GetScreenHeight() const;

    void DrawMeshBuffer( SMeshBuffer& mesh );

private:

    void* mHostAddr;
    gcmContextData* mContext;
};

} // end namespace PS3

#endif // RENDERER_H_INCLUDED

