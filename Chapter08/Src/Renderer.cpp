#include "Renderer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sysutil/video.h>

#include <SDL/SDL.h>

namespace PS3
{

#define DEFUALT_CB_SIZE						0x80000		// 512Kb default command buffer size
#define HOST_STATE_CB_SIZE					0x10000		// 64Kb state command buffer size (used for resetting certain default states)
#define HOST_ADDR_ALIGNMENT					(1024*1024)
#define HOSTBUFFER_SIZE				        (128*1024*1024)

#define FRAME_BUFFER_COUNT					2

extern gcmContextData *context;

extern u32 curr_fb;

extern u32 display_width;
extern u32 display_height;

extern u32 depth_pitch;
extern u32 depth_offset;
extern u32 *depth_buffer;

extern u32 color_pitch;
extern u32 color_offset[FRAME_BUFFER_COUNT];
extern u32 *color_buffer[FRAME_BUFFER_COUNT];

extern f32 aspect_ratio;

void setRenderTarget(u32 index);
void init_screen(void *host_addr,u32 size);
void waitflip();
static inline void flip();

#define GCM_LABEL_INDEX		255

videoResolution vResolution;
gcmContextData *context = NULL;

u32 curr_fb = 0;
u32 first_fb = 1;

u32 display_width;
u32 display_height;

u32 depth_pitch;
u32 depth_offset;
u32 *depth_buffer;

u32 color_pitch;
u32 color_offset[FRAME_BUFFER_COUNT];
u32 *color_buffer[FRAME_BUFFER_COUNT];

f32 aspect_ratio;

static u32 sResolutionIds[] = {
    VIDEO_RESOLUTION_960x1080,
    VIDEO_RESOLUTION_720,
    VIDEO_RESOLUTION_480,
    VIDEO_RESOLUTION_576
};
static size_t RESOLUTION_ID_COUNT = sizeof(sResolutionIds)/sizeof(u32);

static u32 sLabelVal = 1;

//static RSXDebugFontRenderer *debugFontRenderer;

static void waitFinish()
{
	rsxSetWriteBackendLabel(context,GCM_LABEL_INDEX,sLabelVal);

	rsxFlushBuffer(context);

	while(*(vu32*)gcmGetLabelAddress(GCM_LABEL_INDEX)!=sLabelVal)
		usleep(30);

	++sLabelVal;
}

static void waitRSXIdle()
{
	rsxSetWriteBackendLabel(context,GCM_LABEL_INDEX,sLabelVal);
	rsxSetWaitLabel(context,GCM_LABEL_INDEX,sLabelVal);

	++sLabelVal;

	waitFinish();
}

void initVideoConfiguration()
{
    s32 rval = 0;
    s32 resId = 0;

    for (size_t i=0;i < RESOLUTION_ID_COUNT;i++) {
        rval = videoGetResolutionAvailability(VIDEO_PRIMARY, sResolutionIds[i], VIDEO_ASPECT_AUTO, 0);
        if (rval != 1) continue;

        resId = sResolutionIds[i];
        rval = videoGetResolution(resId, &vResolution);
        if(!rval) break;
    }

    if(rval) {
        printf("Error: videoGetResolutionAvailability failed. No usable resolution.\n");
        exit(1);
    }

    videoConfiguration config = {
        (u8)resId,
        VIDEO_BUFFER_FORMAT_XRGB,
        VIDEO_ASPECT_AUTO,
        {0,0,0,0,0,0,0,0,0},
        (u32)vResolution.width*4
    };

    rval = videoConfigure(VIDEO_PRIMARY, &config, NULL, 0);
    if(rval) {
        printf("Error: videoConfigure failed.\n");
        exit(1);
    }

    videoState state;

    rval = videoGetState(VIDEO_PRIMARY, 0, &state);
    switch(state.displayMode.aspect) {
        case VIDEO_ASPECT_4_3:
            aspect_ratio = 4.0f/3.0f;
            break;
        case VIDEO_ASPECT_16_9:
            aspect_ratio = 16.0f/9.0f;
            break;
        default:
            printf("unknown aspect ratio %x\n", state.displayMode.aspect);
            aspect_ratio = 16.0f/9.0f;
            break;
    }

    display_height = vResolution.height;
    display_width = vResolution.width;
}

void setRenderTarget(u32 index)
{
	gcmSurface sf;

	sf.colorFormat		= GCM_SURFACE_X8R8G8B8;
	sf.colorTarget		= GCM_SURFACE_TARGET_0;
	sf.colorLocation[0]	= GCM_LOCATION_RSX;
	sf.colorOffset[0]	= color_offset[index];
	sf.colorPitch[0]	= color_pitch;

	sf.colorLocation[1]	= GCM_LOCATION_RSX;
	sf.colorLocation[2]	= GCM_LOCATION_RSX;
	sf.colorLocation[3]	= GCM_LOCATION_RSX;
	sf.colorOffset[1]	= 0;
	sf.colorOffset[2]	= 0;
	sf.colorOffset[3]	= 0;
	sf.colorPitch[1]	= 64;
	sf.colorPitch[2]	= 64;
	sf.colorPitch[3]	= 64;

	sf.depthFormat		= GCM_SURFACE_ZETA_Z24S8;
	sf.depthLocation	= GCM_LOCATION_RSX;
	sf.depthOffset		= depth_offset;
	sf.depthPitch		= depth_pitch;

	sf.type				= GCM_SURFACE_TYPE_LINEAR;
	sf.antiAlias		= GCM_SURFACE_CENTER_1;

	sf.width			= display_width;
	sf.height			= display_height;
	sf.x				= 0;
	sf.y				= 0;

	rsxSetSurface(context,&sf);
}

void init_screen(void *host_addr,u32 size)
{
    u32 zs_depth = 4;
    u32 color_depth = 4;

	rsxInit(&context,DEFUALT_CB_SIZE,size,host_addr);

	initVideoConfiguration();

	waitRSXIdle();

	gcmSetFlipMode(GCM_FLIP_VSYNC);

	color_pitch = display_width*color_depth;
	depth_pitch = display_width*zs_depth;

	for (u32 i=0;i < FRAME_BUFFER_COUNT;i++) {
		color_buffer[i] = (u32*)rsxMemalign(64,(display_height*color_pitch));
		rsxAddressToOffset(color_buffer[i],&color_offset[i]);
		gcmSetDisplayBuffer(i,color_offset[i],color_pitch,display_width,display_height);
	}

	depth_buffer = (u32*)rsxMemalign(64, display_height*depth_pitch);
	rsxAddressToOffset(depth_buffer,&depth_offset);

	//debugFontRenderer = new RSXDebugFontRenderer(context);
}

void waitflip()
{
	while(gcmGetFlipStatus()!=0)
		usleep(200);
	gcmResetFlipStatus();
}

static inline void flip()
{
	if(!first_fb) waitflip();
	else gcmResetFlipStatus();

	gcmSetFlip(context,curr_fb);
	rsxFlushBuffer(context);

	gcmSetWaitFlip(context);

	curr_fb ^= 1;
	setRenderTarget(curr_fb);

	first_fb = 0;
}

Renderer::Renderer() :
    mHostAddr( nullptr ),
    mContext( nullptr )
{
}

Renderer::~Renderer()
{
}

bool Renderer::Init()
{
    mHostAddr = memalign( HOST_ADDR_ALIGNMENT, HOSTBUFFER_SIZE );
    init_screen( mHostAddr, HOSTBUFFER_SIZE );
    mContext = context; // rsxutil.h
    SetDrawEnv();
    setRenderTarget( curr_fb );
    return true;
}

void Renderer::SetDrawEnv()
{
    rsxSetColorMask(context,GCM_COLOR_MASK_B |
                            GCM_COLOR_MASK_G |
                            GCM_COLOR_MASK_R |
                            GCM_COLOR_MASK_A);

    rsxSetColorMaskMrt(context,0);

    u16 x,y,w,h;
    f32 min, max;
    f32 scale[4],offset[4];

    x = 0;
    y = 0;
    w = display_width;
    h = display_height;
    min = 0.0f;
    max = 1.0f;
    scale[0] = w*0.5f;
    scale[1] = h*-0.5f;
    scale[2] = (max - min)*0.5f;
    scale[3] = 0.0f;
    offset[0] = x + w*0.5f;
    offset[1] = y + h*0.5f;
    offset[2] = (max + min)*0.5f;
    offset[3] = 0.0f;

    rsxSetViewport(context,x, y, w, h, min, max, scale, offset);
    rsxSetScissor(context,x,y,w,h);

    rsxSetDepthTestEnable(context,GCM_TRUE);
    rsxSetDepthFunc(context,GCM_LESS);
    rsxSetShadeModel(context,GCM_SHADE_MODEL_SMOOTH);
    rsxSetDepthWriteEnable(context,1);
    rsxSetFrontFace(context,GCM_FRONTFACE_CCW);
    rsxSetBlendEnable( context, GCM_TRUE ); // enable blending
    rsxSetBlendEquation( context, GCM_FUNC_ADD, GCM_FUNC_ADD ); // default blend equation...
    rsxSetBlendFunc( context, GCM_SRC_ALPHA, // sfcolor (source)
                              GCM_ONE_MINUS_SRC_ALPHA, // dfcolor (destination)
                              GCM_SRC_ALPHA, // sfalpha
                              GCM_ONE_MINUS_SRC_ALPHA ); // dfalpha
}

void Renderer::BeginFrame()
{
    u32 i;
    u32 color = 0xFF555555; // grey screen color (argb)
    SetDrawEnv();
    rsxSetClearColor(context,color);
    rsxSetClearDepthStencil(context,0xffffff00);
    rsxClearSurface(context,GCM_CLEAR_R |
                            GCM_CLEAR_G |
                            GCM_CLEAR_B |
                            GCM_CLEAR_A |
                            GCM_CLEAR_S |
                            GCM_CLEAR_Z);

    rsxSetZMinMaxControl(context,0,1,1);

    for(i=0;i<8;i++)
        rsxSetViewportClip(context,i,display_width,display_height);
}

void Renderer::EndFrame()
{
    flip();
}

int Renderer::GetScreenWidth() const { return display_width; }
int Renderer::GetScreenHeight() const { return display_height; }

void Renderer::DrawMeshBuffer( SMeshBuffer& mesh )
{
    rsxSetUserClipPlaneControl( mContext,GCM_USER_CLIP_PLANE_DISABLE,
                                         GCM_USER_CLIP_PLANE_DISABLE,
                                         GCM_USER_CLIP_PLANE_DISABLE,
                                         GCM_USER_CLIP_PLANE_DISABLE,
                                         GCM_USER_CLIP_PLANE_DISABLE,
                                         GCM_USER_CLIP_PLANE_DISABLE );

    u32 offset = 0;

    //DBG_ASSERT( mesh != nullptr );
    if (
         mesh.cnt_vertices == 0 &&
         mesh.cnt_indices == 0 )

    {
        //DBG_LOG( "mesh buffer null or cnt vertices/indices both 0\n" );
        SDL_Log("mesh cnt vertices and cnt indices both zero");
        return;
    }
    //DBG_LOG( "Rendering, cnt_vertices/indices: %u, %u\n", mesh->cnt_vertices, mesh->cnt_indices );
    rsxAddressToOffset( &mesh.vertices[0].pos, &offset );
    rsxBindVertexArrayAttrib( mContext,
                              GCM_VERTEX_ATTRIB_POS,
                              0,
                              offset,
                              sizeof( S3DVertex ),
                              3,
                              GCM_VERTEX_DATA_TYPE_F32,
                              GCM_LOCATION_RSX );

    //rsxAddressToOffset( &mesh->vertices[0].nrm, &offset );
    //rsxBindVertexArrayAttrib( mContext,
    //                          GCM_VERTEX_ATTRIB_NORMAL,
    //                          0,
    //                          offset,
    //                          sizeof( S3DVertex ),
    //                          3,
    //                          GCM_VERTEX_DATA_TYPE_F32,
    //                          GCM_LOCATION_RSX );

    rsxAddressToOffset( &mesh.vertices[0].u, &offset );
    rsxBindVertexArrayAttrib( mContext,
                              GCM_VERTEX_ATTRIB_TEX0,
                              0,
                              offset,
                              sizeof( S3DVertex ),
                              2,
                              GCM_VERTEX_DATA_TYPE_F32,
                              GCM_LOCATION_RSX );

    if ( mesh.indices != nullptr )
    {
        /*SDL_Log("Vertices:");
        for (int i=0; i < mesh.cnt_indices; ++i)
        {
            SDL_Log("  %f, %f, %f\n",
                    mesh.vertices[mesh.indices[i]].pos.getX(),
                    mesh.vertices[mesh.indices[i]].pos.getY(),
                    mesh.vertices[mesh.indices[i]].pos.getZ());
        }*/

        rsxAddressToOffset( &mesh.indices[0], &offset );
        rsxDrawIndexArray( mContext,
                           GCM_TYPE_TRIANGLES,        // u32 type
                           offset,                    // u32 offset
                           mesh.cnt_indices, // u32 count
                           GCM_INDEX_TYPE_16B,        // u32 data_type
                           GCM_LOCATION_RSX );        // u32 location
    }
    /*else if ( mesh->indices32 != nullptr )
    {
        rsxAddressToOffset( &mesh->indices32[0], &offset );
        rsxDrawIndexArray( mContext,
                           GCM_TYPE_TRIANGLES,        // u32 type
                           offset,                    // u32 offset
                           mesh->cnt_indices, // u32 count
                           GCM_INDEX_TYPE_32B,        // u32 data_type
                           GCM_LOCATION_RSX );        // u32 location
    }*/
    else
    {
        rsxDrawVertexArray( mContext,
                            GCM_TYPE_TRIANGLES,
                            0,
                            mesh.cnt_vertices );
    }
}

/*void Renderer::RenderEntity( Entity& entity )
{
    rsxSetUserClipPlaneControl(context,GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE,
                                       GCM_USER_CLIP_PLANE_DISABLE);
    entity.Render();
}*/

} // end namespace PS3

