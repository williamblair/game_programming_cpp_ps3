// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include <map>
#include <SDL/SDL_scancode.h>
//#include <SDL/SDL_gamecontroller.h>
#include <SDL/SDL_joystick.h>
#include <SDL/SDL_mouse.h>
#include "Math.h"

// How ps3 controller buttons are mapped to SDL buttons
#define PS3_BUTTON_LEFT     0
#define PS3_BUTTON_DOWN     1
#define PS3_BUTTON_RIGHT    2
#define PS3_BUTTON_UP       3
#define PS3_BUTTON_START    4
#define PS3_BUTTON_RANALOG  5
#define PS3_BUTTON_LANALOG  6
#define PS3_BUTTON_SELECT   7
#define PS3_BUTTON_SQUARE   8
#define PS3_BUTTON_CROSS    9
#define PS3_BUTTON_CIRCLE   10
#define PS3_BUTTON_TRIANGLE 11
#define PS3_BUTTON_R1       12
#define PS3_BUTTON_L1       13
#define PS3_BUTTON_R2       14
#define PS3_BUTTON_L2       15

// The different button states
enum ButtonState
{
	ENone,
	EPressed,
	EReleased,
	EHeld
};

// Helper for keyboard input
class KeyboardState
{
public:
	// Friend so InputSystem can easily update it
	friend class InputSystem;
	// Get just the boolean true/false value of key
	bool GetKeyValue(SDL_Scancode keyCode) const;
	// Get a state based on current and previous frame
	ButtonState GetKeyState(SDL_Scancode keyCode) const;
private:
	const Uint8* mCurrState;
	Uint8 mPrevState[SDL_NUM_SCANCODES];
};

// Helper for mouse input
class MouseState
{
public:
	friend class InputSystem;

	// For mouse position
	const Vector2& GetPosition() const { return mMousePos; }
	const Vector2& GetScrollWheel() const { return mScrollWheel; }
	bool IsRelative() const { return mIsRelative; }

	// For buttons
	bool GetButtonValue(int button) const;
	ButtonState GetButtonState(int button) const;
private:
	// Store current mouse position
	Vector2 mMousePos;
	// Motion of scroll wheel
	Vector2 mScrollWheel;
	// Store button data
	Uint32 mCurrButtons;
	Uint32 mPrevButtons;
	// Are we in relative mouse mode
	bool mIsRelative;
};

// Helper for controller input
class ControllerState
{
public:
	friend class InputSystem;
    
    enum { MAX_CONTROLLERS = 4 };

	// For buttons
	bool GetButtonValue(int button) const;
	ButtonState GetButtonState(int button) const;

	const Vector2& GetLeftStick() const { return mLeftStick; }
	const Vector2& GetRightStick() const { return mRightStick; }
	float GetLeftTrigger() const { return mLeftTrigger; }
	float GetRightTrigger() const { return mRightTrigger; }

	bool GetIsConnected() const { return mIsConnected; }
private:
	// Current/previous buttons
	Uint8 mCurrButtons[16]; // arbitrary max number of buttons set currently
	Uint8 mPrevButtons[16];
	// Left/right sticks
	Vector2 mLeftStick;
	Vector2 mRightStick;
	// Left/right trigger
	float mLeftTrigger;
	float mRightTrigger;
	// Is this controller connected?
	bool mIsConnected;
};

// Wrapper that contains current state of input
struct InputState
{
	KeyboardState Keyboard;
	MouseState Mouse;
	ControllerState Controllers[ControllerState::MAX_CONTROLLERS];
};

class InputSystem
{
public:
	bool Initialize();
	void Shutdown();

	// Called right before SDL_PollEvents loop
	void PrepareForUpdate();
	// Called after SDL_PollEvents loop
	void Update();
	// Called to process an SDL event in input system
	void ProcessEvent(union SDL_Event& event);

	const InputState& GetState() const { return mState; }

	void SetRelativeMouseMode(bool value);

    // Exercise 8.2 - event mapping file config
    // returns true on success, false on fail
    bool ParseMappingFile(const std::string& fileName);
    // Exercise 8.2
    // returns state of parsed button for given action;
    // checks all input types (controller, mouse, keyboard)
    ButtonState GetMappedButtonState(const std::string& actionName) const;
private:
	float Filter1D(int input);
	Vector2 Filter2D(int inputX, int inputY);
	InputState mState;
	SDL_Joystick* mControllers[ControllerState::MAX_CONTROLLERS];

    // Configured button mappings for actions
    std::map<std::string, int> mCtrlActionMap;
    std::map<std::string, int> mMouseActionMap;
    std::map<std::string, SDL_Scancode> mKeyboardActionMap;
};

