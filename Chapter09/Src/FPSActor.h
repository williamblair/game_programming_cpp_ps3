// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include "Actor.h"
// TODO
//#include "SoundEvent.h"

class FPSActor : public Actor
{
public:
	FPSActor(class Game* game);

	void UpdateActor(float deltaTime) override;
	void ActorInput(const uint8_t* keys, SDL_Joystick* joy) override;

	void SetFootstepSurface(float value);

	void SetVisible(bool visible);
private:
	class MoveComponent* mMoveComp;
    // TODO
	//class AudioComponent* mAudioComp;
	class MeshComponent* mMeshComp;
	class FPSCamera* mCameraComp;
	class Actor* mFPSModel;
    // TODO
	//SoundEvent mFootstep;
	float mLastFootstep;
};
