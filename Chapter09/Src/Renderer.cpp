// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include <algorithm>
#include <SDL/SDL_image.h>
#include "Renderer.h"
#include "Texture.h"
#include "Mesh.h"
#include "Shader.h"
#include "VertexArray.h"
#include "SpriteComponent.h"
#include "MeshComponent.h"
#include <time.h>

/* Convenience macros for operations on timevals.
   NOTE: `timercmp' does not work for >= or <=.  */
#define    timerisset(tvp)        ((tvp)->tv_sec || (tvp)->tv_usec)
#define    timerclear(tvp)        ((tvp)->tv_sec = (tvp)->tv_usec = 0)
#define    timercmp(a, b, CMP)                               \
  (((a)->tv_sec == (b)->tv_sec) ?                           \
   ((a)->tv_usec CMP (b)->tv_usec) :                           \
   ((a)->tv_sec CMP (b)->tv_sec))
#define    timeradd(a, b, result)                              \
  do {                                          \
    (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;                  \
    (result)->tv_usec = (a)->tv_usec + (b)->tv_usec;                  \
    if ((result)->tv_usec >= 1000000)                          \
      {                                          \
    ++(result)->tv_sec;                              \
    (result)->tv_usec -= 1000000;                          \
      }                                          \
  } while (0)
#define    timersub(a, b, result)                              \
  do {                                          \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;                  \
    (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;                  \
    if ((result)->tv_usec < 0) {                          \
      --(result)->tv_sec;                              \
      (result)->tv_usec += 1000000;                          \
    }                                          \
  } while (0)

float rsxgltest_elapsed_time = 0, rsxgltest_last_time = 0, rsxgltest_delta_time = 0;

struct timeval start_time, current_time;
struct timeval timeout_time = {
    .tv_sec = 6,
    .tv_usec = 0
};
const float ft = 1.0f / 60.0f;
float ft_integral, ft_fractional;
struct timeval frame_time = { 0,0 };

Renderer::Renderer(Game* game)
	:mGame(game)
	,mSpriteShader(nullptr)
	,mMeshShader(nullptr)
{
}

Renderer::~Renderer()
{
}

bool Renderer::Initialize()
{
    mDisplay = eglGetDisplay( EGL_DEFAULT_DISPLAY );
    if ( mDisplay == EGL_NO_DISPLAY )
    {
        SDL_Log( "eglGetDisplay failed: %x\n", eglGetError() );
        return false;
    }

    EGLint version0 = 0;
    EGLint version1 = 0;
    EGLBoolean result = eglInitialize( mDisplay, &version0, &version1 );
    if ( !result )
    {
        SDL_Log( "eglInitialize failed: %x\n", eglGetError() );
        return false;
    }

    SDL_Log( "eglInitialize version: %i %i:%i\n", version0, version1, (int)result );
    EGLint attribs[] = {
        EGL_RED_SIZE,8,
        EGL_BLUE_SIZE,8,
        EGL_GREEN_SIZE,8,
        EGL_ALPHA_SIZE,8,

        EGL_DEPTH_SIZE,16,
        EGL_NONE
    };
    EGLConfig config;
    EGLint nconfig = 0;
    result = eglChooseConfig( mDisplay, attribs, &config, 1, &nconfig );
    SDL_Log( "eglChooseConfig:%i %u configs\n",(int)result,nconfig) ;
    if ( nconfig <= 0 )
    {
        SDL_Log( "egl nconfig <=0\n" );
        result = eglTerminate( mDisplay );
        return false;
    }

    mSurface = eglCreateWindowSurface( mDisplay, config, 0, 0 );
    if ( mSurface == EGL_NO_SURFACE )
    {
        SDL_Log( "eglCreateWindowSurface failed: %x\n",eglGetError() );
        result = eglTerminate( mDisplay );
        return false;
    }

    int width, height;
    eglQuerySurface( mDisplay, mSurface, EGL_WIDTH, &width );
    eglQuerySurface( mDisplay, mSurface, EGL_HEIGHT, &height );
    mScreenWidth = (float)width;
    mScreenHeight = (float)height;

    SDL_Log( "eglCreateWindowSurface: %ix%i\n", width, height );
    
    mCtx = eglCreateContext( mDisplay, config, 0, 0 );
    SDL_Log( "eglCreateContext: %lu\n", (unsigned long)mCtx );

    if ( mCtx == EGL_NO_CONTEXT )
    {
        SDL_Log( "eglCreateContext failed: %x\n", eglGetError() );
        result = eglTerminate( mDisplay );
        return false;
    }

    result = eglMakeCurrent( mDisplay, mSurface, mSurface, mCtx );
    if ( result != EGL_TRUE )
    {
        SDL_Log( "eglMakeCurrent failed: %x\n", eglGetError() );
        result = eglDestroyContext( mDisplay, mCtx );
        result = eglTerminate( mDisplay );
        return false;
    }
    SDL_Log( "eglMakeCurrent\n" );

    if ( SDL_Init(SDL_INIT_JOYSTICK) != 0 )
    {
        SDL_Log( "Unable to initialize SDL: %s", SDL_GetError() );
        return false;
    }
    
    int flags = IMG_INIT_JPG | IMG_INIT_PNG;
    if ( (IMG_Init(flags) & flags) != flags ) {
        SDL_Log( "Failed to init IMG: %s\n", IMG_GetError() );
        return false;
    }

	// Make sure we can create/compile shaders
	if (!LoadShaders())
	{
		SDL_Log("Failed to load shaders.");
		return false;
	}

	// Create quad for drawing sprites
	CreateSpriteVerts();

    ft_fractional = modff(ft,&ft_integral);
    frame_time.tv_sec = (int)ft_integral;
    frame_time.tv_usec = (int)(ft_fractional * 1.0e6);

	return true;
}

void Renderer::Shutdown()
{
	delete mSpriteVerts;
	mSpriteShader->Unload();
	delete mSpriteShader;
	mMeshShader->Unload();
	delete mMeshShader;
    EGLBoolean result = eglDestroyContext( mDisplay, mCtx );
    result = eglTerminate( mDisplay );
}

void Renderer::UnloadData()
{
	// Destroy textures
	for (auto i : mTextures)
	{
		i.second->Unload();
		delete i.second;
	}
	mTextures.clear();

	// Destroy meshes
	for (auto i : mMeshes)
	{
		i.second->Unload();
		delete i.second;
	}
	mMeshes.clear();
}

void Renderer::Draw()
{
    gettimeofday(&current_time,0);

    struct timeval elapsed_time;
    timersub(&current_time,&start_time,&elapsed_time);
    rsxgltest_elapsed_time = ((float)(elapsed_time.tv_sec)) + ((float)(elapsed_time.tv_usec) / 1.0e6f);
    rsxgltest_delta_time = rsxgltest_elapsed_time - rsxgltest_last_time;

    rsxgltest_last_time = rsxgltest_elapsed_time;

	// Set the clear color to light grey
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	// Clear the color buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Draw mesh components
	// Enable depth buffering/disable alpha blend
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	// Set the mesh shader active
	mMeshShader->SetActive();
	// Update view-projection matrix
	mMeshShader->SetMatrixUniform("uViewProj", mView * mProjection);
	// Update lighting uniforms
	SetLightUniforms(mMeshShader);
	for (auto mc : mMeshComps)
	{
		if (mc->GetVisible())
		{
			mc->Draw(mMeshShader);
		}
	}

	// Draw all sprite components
	// Disable depth buffering
	glDisable(GL_DEPTH_TEST);
	// Enable alpha blending on the color buffer
	glEnable(GL_BLEND);
	glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
	glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);

	// Set shader/vao as active
	mSpriteShader->SetActive();
	mSpriteVerts->SetActive();
	for (auto sprite : mSprites)
	{
		if (sprite->GetVisible())
		{
			sprite->Draw( mSpriteShader );
		}
	}

	// Swap the buffers
	//SDL_GL_SwapWindow(mWindow);
    EGLBoolean result = eglSwapBuffers( mDisplay, mSurface );

    EGLint e = eglGetError();
    if ( !result ) {
        SDL_Log("Swap sync timeout: 0x%X\n", e);
    }
    else
    {
        struct timeval t, elapsed_time;
        gettimeofday(&t,0);
        timersub(&t,&current_time,&elapsed_time);

        if(timercmp(&elapsed_time,&frame_time,<)) {
            struct timeval sleep_time;
            timersub(&frame_time,&elapsed_time,&sleep_time);
            usleep((sleep_time.tv_sec * 1e6) + sleep_time.tv_usec);
        }

        // TODO
        // sysUtilCheckCallback();
    }
}

void Renderer::AddSprite(SpriteComponent* sprite)
{
	// Find the insertion point in the sorted vector
	// (The first element with a higher draw order than me)
	int myDrawOrder = sprite->GetDrawOrder();
	auto iter = mSprites.begin();
	for (;
		iter != mSprites.end();
		++iter)
	{
		if (myDrawOrder < (*iter)->GetDrawOrder())
		{
			break;
		}
	}

	// Inserts element before position of iterator
	mSprites.insert(iter, sprite);
}

void Renderer::RemoveSprite(SpriteComponent* sprite)
{
	auto iter = std::find(mSprites.begin(), mSprites.end(), sprite);
	mSprites.erase(iter);
}

void Renderer::AddMeshComp(MeshComponent* mesh)
{
	mMeshComps.emplace_back(mesh);
}

void Renderer::RemoveMeshComp(MeshComponent* mesh)
{
	auto iter = std::find(mMeshComps.begin(), mMeshComps.end(), mesh);
	mMeshComps.erase(iter);
}

Texture* Renderer::GetTexture(const std::string& fileName)
{
	Texture* tex = nullptr;
	auto iter = mTextures.find(fileName);
	if (iter != mTextures.end())
	{
		tex = iter->second;
	}
	else
	{
		tex = new Texture();
		if (tex->Load(fileName))
		{
			mTextures.emplace(fileName, tex);
		}
		else
		{
			delete tex;
			tex = nullptr;
		}
	}
	return tex;
}

Mesh* Renderer::GetMesh(const std::string & fileName)
{
	Mesh* m = nullptr;
	auto iter = mMeshes.find(fileName);
	if (iter != mMeshes.end())
	{
		m = iter->second;
	}
	else
	{
		m = new Mesh();
		if (m->Load(fileName, this))
		{
			mMeshes.emplace(fileName, m);
		}
		else
		{
			delete m;
			m = nullptr;
		}
	}
	return m;
}

static inline void PrintMat4(Matrix4& mat)
{
    const float* ptr = mat.GetAsFloatPtr();
    for (int i=0; i<4; ++i)
    {
        for (int j=0; j<4; ++j)
        {
            SDL_Log("%08.08f ", *ptr++);
        }
        SDL_Log("\n");
    }
    SDL_Log("\n");
}

bool Renderer::LoadShaders()
{
	// Create sprite shader
	mSpriteShader = new Shader();
	if (!mSpriteShader->Load(ASSETS_DIR"/Shaders/Sprite.vert", ASSETS_DIR"/Shaders/Sprite.frag"))
	{
		return false;
	}

	mSpriteShader->SetActive();
	// Set the view-projection matrix
	Matrix4 viewProj = Matrix4::CreateSimpleViewProj(mScreenWidth, mScreenHeight);
    //SDL_Log("viewProj:\n");
    //PrintMat4( viewProj );
	mSpriteShader->SetMatrixUniform("uViewProj", viewProj);

	// Create basic mesh shader
	mMeshShader = new Shader();
	if (!mMeshShader->Load(ASSETS_DIR"/Shaders/Phong.vert", ASSETS_DIR"/Shaders/Phong.frag"))
	{
		return false;
	}

	mMeshShader->SetActive();
	// Set the view-projection matrix
	mView = Matrix4::CreateLookAt(Vector3::Zero, Vector3::UnitX, Vector3::UnitZ);
	mProjection = Matrix4::CreatePerspectiveFOV(Math::ToRadians(70.0f),
		mScreenWidth, mScreenHeight, 10.0f, 10000.0f);
	mMeshShader->SetMatrixUniform("uViewProj", mView * mProjection);
	return true;
}

void Renderer::CreateSpriteVerts()
{
	float vertices[] = {
		-0.5f, 0.5f, 0.f, 0.f, 0.f, 0.0f, 0.f, 0.f, // top left
		0.5f, 0.5f, 0.f, 0.f, 0.f, 0.0f, 1.f, 0.f, // top right
		0.5f,-0.5f, 0.f, 0.f, 0.f, 0.0f, 1.f, 1.f, // bottom right
		-0.5f,-0.5f, 0.f, 0.f, 0.f, 0.0f, 0.f, 1.f  // bottom left
	};

	unsigned int indices[] = {
		0, 1, 2,
		2, 3, 0
	};

	mSpriteVerts = new VertexArray(vertices, 4, indices, 6);
}

void Renderer::SetLightUniforms(Shader* shader)
{
	// Camera position is from inverted view
	Matrix4 invView = mView;
	invView.Invert();
	shader->SetVectorUniform("uCameraPos", invView.GetTranslation());
	// Ambient light
	shader->SetVectorUniform("uAmbientLight", mAmbientLight);
	// Directional light
	shader->SetVectorUniform("uDirLight.mDirection",
		mDirLight.mDirection);
	shader->SetVectorUniform("uDirLight.mDiffuseColor",
		mDirLight.mDiffuseColor);
	shader->SetVectorUniform("uDirLight.mSpecColor",
		mDirLight.mSpecColor);
}

Vector3 Renderer::Unproject(const Vector3& screenPoint) const
{
	// Convert screenPoint to device coordinates (between -1 and +1)
	Vector3 deviceCoord = screenPoint;
	deviceCoord.x /= (mScreenWidth) * 0.5f;
	deviceCoord.y /= (mScreenHeight) * 0.5f;

	// Transform vector by unprojection matrix
	Matrix4 unprojection = mView * mProjection;
	unprojection.Invert();
	return Vector3::TransformWithPerspDiv(deviceCoord, unprojection);
}

void Renderer::GetScreenDirection(Vector3& outStart, Vector3& outDir) const
{
	// Get start point (in center of screen on near plane)
	Vector3 screenPoint(0.0f, 0.0f, 0.0f);
	outStart = Unproject(screenPoint);
	// Get end point (in center of screen, between near and far)
	screenPoint.z = 0.9f;
	Vector3 end = Unproject(screenPoint);
	// Get direction vector
	outDir = end - outStart;
	outDir.Normalize();
}

