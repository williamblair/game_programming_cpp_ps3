#ifndef __PS3_INPUT_H__
#define __PS3_INPUT_H__

// How ps3 controller buttons are mapped to SDL buttons
#define PS3_BUTTON_LEFT     0
#define PS3_BUTTON_DOWN     1
#define PS3_BUTTON_RIGHT    2
#define PS3_BUTTON_UP       3
#define PS3_BUTTON_START    4
#define PS3_BUTTON_RANALOG  5
#define PS3_BUTTON_LANALOG  6
#define PS3_BUTTON_SELECT   7
#define PS3_BUTTON_SQUARE   8
#define PS3_BUTTON_CROSS    9
#define PS3_BUTTON_CIRCLE   10
#define PS3_BUTTON_TRIANGLE 11
#define PS3_BUTTON_R1       12
#define PS3_BUTTON_L1       13
#define PS3_BUTTON_R2       14
#define PS3_BUTTON_L2       15

#endif // __PS3_INPUT_H__

