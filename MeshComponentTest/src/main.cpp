/*
 * rsxgltest - host code
 *
 * I promise this won't become a rewrite of GLUT. In fact, I plan to switch to SDL soon.
 */

#include <EGL/egl.h>
#define GL3_PROTOTYPES
#include <GL3/gl3.h>
#include <GL3/rsxgl.h>
#include <GL3/rsxgl3ext.h>

#include <net/net.h>
#include <sysutil/sysutil.h>
#include <io/pad.h>

#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#ifndef M_PI
#define M_PI 3.1415926535897932384626433
#endif

#include <time.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdarg.h>

#include "rsxgl_config.h"
#include <rsx/commands.h>

#include "math3d.h"
#include "Shader.h"
#include "Renderer.h"
#include "VertexArray.h"
#include "Texture.h"
#include "SpriteComponent.h"
#include "MeshComponent.h"

static char meshVertShader[] =
"attribute vec3 inPosition;\n"
"attribute vec3 inNormal;\n"
"attribute vec2 inTexCoord;\n"
"\n"
"uniform mat4 uWorldTransform;\n"
"uniform mat4 uViewProj;\n"
"\n"
"varying vec2 fragTexCoord;\n"
"varying vec3 fragNormal;\n"
"varying vec3 fragWorldPos;\n"
"\n"
"void main()\n"
"{\n"
"   vec4 pos = vec4(inPosition, 1.0);\n"
"   pos = uWorldTransform * pos;\n"
"   fragWorldPos = pos.xyz;\n"
"   gl_Position = uViewProj * pos;\n"
"\n"
"   fragNormal = (uWorldTransform * vec4(inNormal, 0.0f)).xyz;\n"
"   fragTexCoord = inTexCoord;\n"
"}\n";

static char meshFragShader[] = 
"varying vec2 fragTexCoord;\n"
"varying vec3 fragNormal;\n"
"varying vec3 fragWorldPos;\n"
"\n"
"uniform sampler2D uTexture;\n"
"uniform vec3 uCameraPos;\n"
"uniform float uSpecPower;\n"
"uniform vec3 uAmbientLight;\n"
"\n"
"struct DirectionalLight\n"
"{\n"
"   vec3 mDirection;\n"
"   vec3 mDiffuseColor;\n"
"   vec3 mSpecColor;\n"
"};\n"
"\n"
"uniform DirectionalLight uDirLight;\n"
"\n"
"void main()\n"
"{\n"
"   vec3 N = normalize(fragNormal);\n"
"   vec3 L = normalize(-uDirLight.mDirection);\n"
"   vec3 V = normalize(uCameraPos - fragWorldPos);\n"
"   vec3 R = normalize(reflect(-L,N));\n"
"\n"
"   vec3 Phong = uAmbientLight;\n"
"   float NdotL = dot(N, L);\n"
"   vec3 Diffuse = clamp(uDirLight.mDiffuseColor * NdotL, 0.0f, 1.0f);\n"
"   vec3 Specular = clamp( uDirLight.mSpecColor * \n"
"                               pow(max(0.0f, dot(R,V)), uSpecPower), \n"
"                          0.0f, 1.0f );\n"
"   Phong += Diffuse + Specular;\n"
"   gl_FragColor = texture2D(uTexture, fragTexCoord) * vec4(Phong, 1.0f);\n"
"}\n";

static const char sprite_vertex_shader[] = 
"attribute vec3 inPosition;\n"
"attribute vec3 inNormal;\n"
"attribute vec2 inTexCoord;\n"
"\n"
"uniform mat4 uWorldTransform;\n"
"uniform mat4 uViewProj;\n"
"\n"
"varying vec2 fragTexCoord;"
"\n"
"void main()\n"
"{\n"
"   // Convert position to homogenous coordinates\n"
"   vec4 pos = vec4(inPosition, 1.0);\n"
"   // Transform to position world space, then clip space\n"
"   gl_Position = uViewProj * uWorldTransform * pos;\n"
"\n"
"   // Pass along the texture coordinate to the frag shader\n"
"   fragTexCoord = inTexCoord;\n"
"}\n";

static const char sprite_fragment_shader[] = 
"varying vec2 fragTexCoord;\n"
"\n"
"uniform sampler2D uTexture;\n"
"\n"
"void main(void)\n"
"{\n"
"   //vec4 orig = texture2D(uTexture, fragTexCoord);\n"
"   //gl_FragColor = vec4(orig.a, orig.b, orig.g, orig.r);\n"
"   gl_FragColor = texture2D(uTexture, fragTexCoord);\n"
"}\n";


// quit;
int running = 1, drawing = 1;

static void
eventHandle(u64 status, u64 param, void * userdata) {
  (void)param;
  (void)userdata;
  if(status == SYSUTIL_EXIT_GAME){
    //printf("Quit app requested\n");
    //exit(0);
    running = 0;
  }
  else if(status == SYSUTIL_MENU_OPEN) {
    drawing = 0;
  }
  else if(status == SYSUTIL_MENU_CLOSE) {
    drawing = 1;
  }
  else {
    //printf("Unhandled event: %08llX\n", (unsigned long long int)status);
  }
}

void
appCleanup()
{
  sysUtilUnregisterCallback(SYSUTIL_EVENT_SLOT0);
  netDeinitialize();
  printf("Exiting for real.\n");
}

/* Convenience macros for operations on timevals.
   NOTE: `timercmp' does not work for >= or <=.  */
#define    timerisset(tvp)        ((tvp)->tv_sec || (tvp)->tv_usec)
#define    timerclear(tvp)        ((tvp)->tv_sec = (tvp)->tv_usec = 0)
#define    timercmp(a, b, CMP)                               \
  (((a)->tv_sec == (b)->tv_sec) ?                           \
   ((a)->tv_usec CMP (b)->tv_usec) :                           \
   ((a)->tv_sec CMP (b)->tv_sec))
#define    timeradd(a, b, result)                              \
  do {                                          \
    (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;                  \
    (result)->tv_usec = (a)->tv_usec + (b)->tv_usec;                  \
    if ((result)->tv_usec >= 1000000)                          \
      {                                          \
    ++(result)->tv_sec;                              \
    (result)->tv_usec -= 1000000;                          \
      }                                          \
  } while (0)
#define    timersub(a, b, result)                              \
  do {                                          \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;                  \
    (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;                  \
    if ((result)->tv_usec < 0) {                          \
      --(result)->tv_sec;                              \
      (result)->tv_usec += 1000000;                          \
    }                                          \
  } while (0)


int gScreenWidth = 0;
int gScreenHeight = 0;
void DrawMesh( Shader* shader, MeshComponent* mesh,
                Shader& spriteShader, VertexArray* va, SpriteComponent* sprite )
{
    static Matrix4 modelMatrix;
    static Matrix4 viewMatrix = Matrix4::CreateLookAt( Vector3::Zero, // position
        Vector3::UnitX, // target
        Vector3::UnitZ ); // up
    static Matrix4 projMatrix;
    static Matrix4 viewProjMatrix;

    static Matrix4 spriteProjMat;

    static float lightDir = -1.0f;
    static float lightDirSpeed = 0.01f;

    lightDir += lightDirSpeed;
    if ( lightDir > 1.0f ) {
        lightDirSpeed = -0.01f;
        lightDir = 1.0f;
    }
    else if ( lightDir < -1.0f ) {
        lightDirSpeed = 0.01f;
        lightDir = -1.0f;
    }

    glClearColor( 1.0f, 0.0f, 1.0f, 0.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable( GL_DEPTH_TEST );
    glDisable( GL_BLEND );

    shader->SetActive();
    
    projMatrix = Matrix4::CreatePerspectiveFOV( Math::ToRadians(70.0f),
        gScreenWidth, gScreenHeight, 10.0f, 10000.0f );

    viewProjMatrix = viewMatrix * projMatrix;
    shader->SetMatrixUniform( "uViewProj", viewProjMatrix.GetAsFloatPtr() );

    modelMatrix = Matrix4::CreateScale( 3.0f );
    modelMatrix *= Matrix4::CreateTranslation( Vector3( 200.0f, -75.0f, 0.0f ) );
    shader->SetMatrixUniform( "uWorldTransform", modelMatrix.GetAsFloatPtr() );

    // Set light uniforms
    //{
        // Camera position from inverted view
        Matrix4 invView = viewMatrix;
        invView.Invert();
        Vector3 viewPos = invView.GetTranslation();
        shader->SetVectorUniform( "uCameraPos", viewPos.GetAsFloatPtr() );

        // ambient light
        Vector3 ambLight( 0.2f, 0.2f, 0.2f );
        shader->SetVectorUniform( "uAmbientLight", ambLight.GetAsFloatPtr() );

        Vector3 specColor( 0.8f, 0.8f, 0.8f );
        shader->SetVectorUniform( "uSpecColor", specColor.GetAsFloatPtr() );

        // Directional light
        Vector3 dirLightDir( 0.5f, -1.0f, lightDir );
        dirLightDir.Normalize();
        shader->SetVectorUniform( "uDirLight.mDirection", dirLightDir.GetAsFloatPtr() );
        Vector3 dirLightDiffuse( 0.78f, 0.88f, 1.0f );
        shader->SetVectorUniform( "uDirLight.mDiffuseColor", dirLightDiffuse.GetAsFloatPtr() );
    //}

    mesh->Draw( shader );

    glDisable( GL_DEPTH_TEST );
    spriteProjMat = Matrix4::CreateSimpleViewProj( gScreenWidth, gScreenHeight );
    spriteShader.SetActive();
    spriteShader.SetMatrixUniform( "uViewProj", spriteProjMat.GetAsFloatPtr() );
    spriteShader.SetIntUniform( "uTexture", 0 ); // GL_TEXTURE0

    va->SetActive();
    sprite->Draw( &spriteShader );
    
}

bool InitSprite( Shader* shader, 
                 Texture*& texture,
                 VertexArray*& va,
                 SpriteComponent*& sprite )
{
    texture = new Texture();
    if ( !texture ) {
        printf( "Failed to alloc texture\n" );
        return false;
    }
    if ( !texture->Load(ASSETS_DIR"/HealthBar.png") ) {
        return false;
    }
    sprite = new SpriteComponent();
    sprite->SetTexture( texture );
    sprite->SetPosition( Vector3(-350.0f, -350.0f, 0.0f) );
    sprite->SetVisible( true );

    float vertices[] = {
        -0.5f, 0.5f, 0.f, 0.f, 0.f, 0.0f, 0.f, 0.f, // top left
        0.5f, 0.5f, 0.f, 0.f, 0.f, 0.0f, 1.f, 0.f, // top right
        0.5f,-0.5f, 0.f, 0.f, 0.f, 0.0f, 1.f, 1.f, // bottom right
        -0.5f,-0.5f, 0.f, 0.f, 0.f, 0.0f, 0.f, 1.f  // bottom left
    };
    unsigned int indices[] = {
        0, 1, 2,
        2, 3, 0
    };
    va = new VertexArray( vertices, 4, indices, 6 );
    if ( !va ) {
        printf("Failed to alloc sprite vertex array\n");
        return false;
    }

    return true;
}

int
main(int argc, const char ** argv)
{
    // Test program might want to use these:
    int rsxgltest_width = 0, rsxgltest_height = 0;
    float rsxgltest_elapsed_time = 0, rsxgltest_last_time = 0, rsxgltest_delta_time = 0;
    
    Shader spriteShader;
    VertexArray* spriteVertArray = nullptr;
    Shader shader;
    Renderer renderer;
    MeshComponent* meshComp = nullptr;
    SpriteComponent* spriteComp = nullptr;
    Texture* spriteTex = nullptr;

    netInitialize();

    //ioPadInit(1);
    padInfo padinfo;
    padData paddata;

    if ( !renderer.Init() )
    {
        return 1;
    }
    gScreenWidth = renderer.GetWidth();
    gScreenHeight = renderer.GetHeight();

    if ( !spriteShader.Load( std::string(sprite_vertex_shader),
                             std::string(sprite_fragment_shader) ))
    {
        return 1;
    }
    spriteShader.SetActive();
    if ( !InitSprite( &spriteShader, spriteTex, spriteVertArray, spriteComp )) {
        return 1;
    }

    if ( !shader.Load( std::string(meshVertShader),
                       std::string(meshFragShader) ))
    {
        return 1;
    }
    shader.SetActive();

    meshComp = new MeshComponent();
    if ( !meshComp ) {
        SDL_LogError( 0, "Failed to alloc mesh component" );
        return 1;
    }
    {
        Mesh* m = renderer.GetMesh( ASSETS_DIR"/Assets/Sphere.gpmesh" );
        if ( !m ) {
            SDL_LogError( 0, "Failed to get sphere gpmesh" );
            delete meshComp;
            return 1;
        }
        meshComp->SetMesh( m );
    }

    atexit( appCleanup );
    sysUtilRegisterCallback( SYSUTIL_EVENT_SLOT0, eventHandle, NULL );

    struct timeval start_time, current_time;
    struct timeval timeout_time = {
        .tv_sec = 6,
        .tv_usec = 0
    };

    gettimeofday(&start_time,0);
    rsxgltest_last_time = 0.0f;

    // convert to a timeval structure:
    const float ft = 1.0f / 60.0f;
    float ft_integral, ft_fractional;
    ft_fractional = modff(ft,&ft_integral);
    struct timeval frame_time = { 0,0 };
    frame_time.tv_sec = (int)ft_integral;
    frame_time.tv_usec = (int)(ft_fractional * 1.0e6);

    while ( running )
    {
        gettimeofday(&current_time,0);

        struct timeval elapsed_time;
        timersub(&current_time,&start_time,&elapsed_time);
        rsxgltest_elapsed_time = ((float)(elapsed_time.tv_sec)) + ((float)(elapsed_time.tv_usec) / 1.0e6f);
        rsxgltest_delta_time = rsxgltest_elapsed_time - rsxgltest_last_time;

        rsxgltest_last_time = rsxgltest_elapsed_time;

        ioPadGetInfo(&padinfo);
        for(size_t i = 0;i < MAX_PADS;++i) {
            if(padinfo.status[i]) {
                 ioPadGetData(i,&paddata);
                //rsxgltest_pad(i,&paddata);
                if ( paddata.BTN_START ) {
                    running = false;
                }
                break;
            }
        }

        if(drawing) {
            //EGLBoolean result = rsxgltest_draw();
            //DrawTriangle( shader, triangleArray );
            //DrawSprite( shader, spriteVertArray, spriteComp );
            DrawMesh( &shader, meshComp,
                      spriteShader, spriteVertArray, spriteComp );
            EGLBoolean result = true;
            if(!result) break;
        }

        EGLBoolean result = renderer.Update();

        EGLint e = eglGetError();
        if ( !result ) {
            printf("Swap sync timed-out: %x\n",e);
            break;
        }
        else {
            struct timeval t, elapsed_time;
            gettimeofday(&t,0);
            timersub(&t,&current_time,&elapsed_time);

            if(timercmp(&elapsed_time,&frame_time,<)) {
                struct timeval sleep_time;
                timersub(&frame_time,&elapsed_time,&sleep_time);
                usleep((sleep_time.tv_sec * 1e6) + sleep_time.tv_usec);
            }

            sysUtilCheckCallback();
        }
    }

    printf("renderer shutdown\n");
    if ( meshComp != nullptr ) delete meshComp;
    renderer.Shutdown();

    return 0;
}

