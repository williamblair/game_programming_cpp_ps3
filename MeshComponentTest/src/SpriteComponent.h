// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
//#include "Component.h"
#include "Math.h"
#include <SDL/SDL.h>
class SpriteComponent/* : public Component*/
{
public:
    // (Lower draw order corresponds with further back)
    SpriteComponent(/*class Actor* owner, int drawOrder = 100*/);
    virtual ~SpriteComponent();

    virtual void Draw(class Shader* shader);
    virtual void SetTexture(class Texture* texture);

    //int GetDrawOrder() const { return mDrawOrder; }
    int GetTexHeight() const { return mTexHeight; }
    int GetTexWidth() const { return mTexWidth; }

    void SetVisible(bool visible) { mVisible = visible; }
    bool GetVisible() const { return mVisible; }

    void SetPosition( Vector3& position ) { mPosition = position; }
    void SetPosition( Vector3 position ) { mPosition = position; }

protected:
    class Texture* mTexture;
    Vector3 mPosition;
    //int mDrawOrder;
    int mTexWidth;
    int mTexHeight;
    bool mVisible;
};

