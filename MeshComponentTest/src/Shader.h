// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#pragma once
#include <EGL/egl.h>
#define GL3_PROTOTYPES
#include <GL3/gl3.h>
#include <GL3/rsxgl.h>
#include <GL3/rsxgl3ext.h>
#include <string>
#include <cstring>

class Shader
{
public:
    Shader();
    ~Shader();
    // Load the vertex/fragment shaders with the given contents
    bool Load(const std::string& vertContents, const std::string& fragContents);
    void Unload();
    // Set this as the active shader program
    void SetActive();
    // Sets a Matrix uniform
    void SetMatrixUniform( const char* name, const GLfloat* matrix );
    // Sets a Vector3 uniform
    void SetVectorUniform( const char* name, const GLfloat* vector );
    // Sets a float uniform
    void SetFloatUniform( const char* name, GLfloat value );
    // Sets an int uniform
    void SetIntUniform( const char* name, GLint value );

    GLuint GetProgram() const { return mShaderProgram; }
private:

    // Tries to compile the specified shader
    bool CompileShader(const std::string& fileContents,
                       GLenum shaderType,
                       GLuint& outShader);
    
    // Tests whether shader compiled successfully
    bool IsCompiled(GLuint shader);
    // Tests whether vertex/fragment programs link
    bool IsValidProgram();
private:
    // Store the shader object IDs
    GLuint mVertexShader;
    GLuint mFragShader;
    GLuint mShaderProgram;
};

