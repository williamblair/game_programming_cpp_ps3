// ----------------------------------------------------------------
// From Game Programming in C++ by Sanjay Madhav
// Copyright (C) 2017 Sanjay Madhav. All rights reserved.
// 
// Released under the BSD License
// See LICENSE in root directory for full details.
// ----------------------------------------------------------------

#include "VertexArray.h"
#include <EGL/egl.h>
#define GL3_PROTOTYPES
#include <GL3/gl3.h>
#include <GL3/rsxgl.h>
#include <GL3/rsxgl3ext.h>
#include "Shader.h"

VertexArray::VertexArray(/*Shader* shader, */const float* verts, unsigned int numVerts,
    const unsigned int* indices, unsigned int numIndices)
    :mNumVerts(numVerts)
    ,mNumIndices(numIndices)
{
    //shader->SetActive();

    // Create vertex array
    //glGenVertexArrays(1, &mVertexArray);
    //glBindVertexArray(mVertexArray);

    // Create vertex buffer
    // Assumes 3 floats per vertex (x,y,z position only currently)
    glGenBuffers(1, &mVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, numVerts * 8 * sizeof(float), verts, GL_STATIC_DRAW);


    // Create index buffer
    glGenBuffers(1, &mIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);

    //GLuint program = shader->GetProgram();
    GLint vertAttribLoc = /*glGetAttribLocation( program, "inPosition" )*/0;
    GLint normAttribLoc = /*glGetAttribLocation( program, "inNormal" )*/1;
    GLint uvAttribLoc = /*glGetAttribLocation( program, "inTexCoord" )*/2;
    if ( vertAttribLoc < 0 || normAttribLoc < 0 || uvAttribLoc < 0 ) {
        printf("ERROR - failed to get vert, norm, or uv attrib locations!\n");
    }

    // Specify the vertex attributes
    // (For now, assume one vertex format)
    // Position is 3 floats
    glEnableVertexAttribArray(vertAttribLoc);
    glVertexAttribPointer(vertAttribLoc, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);

    // Normal is 3 floats
    glEnableVertexAttribArray(normAttribLoc);
    glVertexAttribPointer(normAttribLoc, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
        reinterpret_cast<void*>(sizeof(float) * 3));
    // Texture coordinates is 2 floats
    glEnableVertexAttribArray(uvAttribLoc);
    glVertexAttribPointer(uvAttribLoc, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
        reinterpret_cast<void*>(sizeof(float) * 6));
}

VertexArray::~VertexArray()
{
    glDeleteBuffers(1, &mVertexBuffer);
    glDeleteBuffers(1, &mIndexBuffer);
    glDeleteVertexArrays(1, &mVertexArray);
}

void VertexArray::SetActive()
{
    // Create vertex buffer
    // Assumes 3 floats per vertex (x,y,z position only currently)
    //glGenBuffers(1, &mVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
    //glBufferData(GL_ARRAY_BUFFER, numVerts * 8 * sizeof(float), verts, GL_STATIC_DRAW);
    //glBindVertexArray(mVertexArray);
    // Create index buffer
    //glGenBuffers(1, &mIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);

    //GLuint program = shader->GetProgram();
    GLint vertAttribLoc = /*glGetAttribLocation( program, "inPosition" )*/0;
    GLint normAttribLoc = /*glGetAttribLocation( program, "inNormal" )*/1;
    GLint uvAttribLoc = /*glGetAttribLocation( program, "inTexCoord" )*/2;
    if ( vertAttribLoc < 0 || normAttribLoc < 0 || uvAttribLoc < 0 ) {
        printf("ERROR - failed to get vert, norm, or uv attrib locations!\n");
    }

    // Specify the vertex attributes
    // (For now, assume one vertex format)
    // Position is 3 floats
    glEnableVertexAttribArray(vertAttribLoc);
    glVertexAttribPointer(vertAttribLoc, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);

    // Normal is 3 floats
    glEnableVertexAttribArray(normAttribLoc);
    glVertexAttribPointer(normAttribLoc, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
        reinterpret_cast<void*>(sizeof(float) * 3));
    // Texture coordinates is 2 floats
    glEnableVertexAttribArray(uvAttribLoc);
    glVertexAttribPointer(uvAttribLoc, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
        reinterpret_cast<void*>(sizeof(float) * 6));
}

void VertexArray::Draw()
{
    SetActive();
    glDrawElements( GL_TRIANGLES, GetNumIndices(), GL_UNSIGNED_INT, nullptr );
}

