#include <SDL/SDL.h>

int main(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("Failed to init SDL\n");
        return 1;
    }

    SDL_Surface* window = SDL_SetVideoMode(1024, 768, 0, SDL_FULLSCREEN);
    if (!window) {
        printf("Failed to create window: %s\n", SDL_GetError());
        return 1;
    }

    bool running = true;
    while (running)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) {
                running = false;
            }
        }

        SDL_FillRect(window, NULL, SDL_MapRGB(window->format, 0,255,0));
        SDL_Flip(window);

        SDL_Delay(1000.0f/60.0f);
    }

    SDL_FreeSurface(window);
    SDL_Quit();

    return 0;
}

